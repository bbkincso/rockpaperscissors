﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace StonePaperScissors
{
    [Activity(Label = "EditBackgroundActivity")]
    public class EditBackgroundActivity : Activity
    {
        RadioButton brown, green, blue, red, yellow;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.EditBackground);


            brown = FindViewById<RadioButton>(Resource.Id.rdbBrown);
            green = FindViewById<RadioButton>(Resource.Id.rdbGreen);
            blue = FindViewById<RadioButton>(Resource.Id.rdbBlue);
            red = FindViewById<RadioButton>(Resource.Id.rdbRed);
            yellow = FindViewById<RadioButton>(Resource.Id.rdbYellow);


            brown.Click += RadioButtonClick;
            green.Click += RadioButtonClick;
            blue.Click += RadioButtonClick;
            red.Click += RadioButtonClick;
            yellow.Click += RadioButtonClick;
            

        }

        private void RadioButtonClick(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            Intent updatedBackground = new Intent();
            updatedBackground.PutExtra("newBackground", rb.Text);
            SetResult(Result.Ok, updatedBackground);
            Finish();

        }

    }
}
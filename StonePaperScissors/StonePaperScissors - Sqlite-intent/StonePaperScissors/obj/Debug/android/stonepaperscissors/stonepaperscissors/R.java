/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package stonepaperscissors.stonepaperscissors;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int bluebackground=0x7f020000;
        public static final int brownbackground=0x7f020001;
        public static final int exit=0x7f020002;
        public static final int gear=0x7f020003;
        public static final int googleplay1=0x7f020004;
        public static final int greenbackground=0x7f020005;
        public static final int paper2=0x7f020006;
        public static final int playbuttonstyle=0x7f020007;
        public static final int redbackground=0x7f020008;
        public static final int scissors=0x7f020009;
        public static final int stone=0x7f02000a;
        public static final int yellowbackground=0x7f02000b;
    }
    public static final class id {
        public static final int btnPlay=0x7f06000b;
        public static final int imgComp=0x7f06000c;
        public static final int imgEdit=0x7f060011;
        public static final int imgExit=0x7f060008;
        public static final int imgGooglePlay=0x7f060007;
        public static final int imgPaper=0x7f06000e;
        public static final int imgPlayer=0x7f06000a;
        public static final int imgScissors=0x7f06000f;
        public static final int imgStone=0x7f06000d;
        public static final int lblDisplay=0x7f060012;
        public static final int lblRockPaperScissors=0x7f060009;
        public static final int lblScoreBoard=0x7f060010;
        public static final int llRoot=0x7f060006;
        public static final int rdbBlue=0x7f060003;
        public static final int rdbBrown=0x7f060001;
        public static final int rdbGreen=0x7f060005;
        public static final int rdbGroup=0x7f060000;
        public static final int rdbRed=0x7f060002;
        public static final int rdbYellow=0x7f060004;
    }
    public static final class layout {
        public static final int editbackground=0x7f030000;
        public static final int main=0x7f030001;
        public static final int scoreboard=0x7f030002;
    }
    public static final class string {
        public static final int app_name=0x7f040000;
    }
    public static final class style {
        public static final int RPSGameTheme=0x7f050000;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace StonePaperScissors.DataAccess
{
    [Table("Scores")]
    public class Scores
    {
        
        [PrimaryKey, AutoIncrement]
        public int ScoreID { get; set; }
        public string Date { get; set; }
        public int Score { get; set; }

        public Scores(int id, string date, int score)
        {
            Date = date;
            Score = score;
            ScoreID=id;
        }

        public Scores()
        {

        }

        public override string ToString()
        {
            return "score: " + Score + "\t\t" + Date;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using StonePaperScissors.DataAccess;



namespace StonePaperScissors
{
    [Activity(Label = "ScoreBoardActivity")]
    public class ScoreBoardActivity : Activity
    {
        string dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "RockPaperScissors.db3");
        TextView displayText;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ScoreBoard);

            displayText = FindViewById<TextView>(Resource.Id.lblDisplay);

            Display();

        }

        public void GetTop5()
        {
            var db = new SQLiteConnection(dbPath);

            var record = (from records in db.Table<Scores>()
                          orderby records.Score descending
                          select records);

            foreach (var item in record)
            {
                Scores newScore = new Scores(item.ScoreID, item.Date, item.Score);
                displayText.Text += newScore.ToString() + "\n";
            }

        }
        public void Display()
        {
            var db = new SQLiteConnection(dbPath);

            var allRecords = db.Table<Scores>().Count();

            if (allRecords == 0)
            {
                displayText.Text = "There is no record.";
            }

            GetTop5();
        }

    }
}
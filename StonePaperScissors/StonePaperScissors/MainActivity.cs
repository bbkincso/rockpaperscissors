﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using Android.Content.PM;
using System;
using Android.Runtime;
using SQLite;
using System.IO;
using StonePaperScissors.DataAccess;
using System.Collections.Generic;
using Android.Media;

namespace StonePaperScissors
{
    [Activity(Label = "StonePaperScissors", MainLauncher = true, ScreenOrientation = ScreenOrientation.Landscape)]
    public class MainActivity : Activity

    {

        RelativeLayout root;
        string userchoice, computerchoice;
        ImageView player, computer, paper, stone, scissors, edit, scoreBoard;
        Button play;
        Random random = new Random();
        int playerScore = 0;
        int myLives = 3;
        TextView currentScore, currentLives;
        string currentDate = DateTime.Now.ToString("yyyy-MM-dd");
        string dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "RockPaperScissors.db3");
        readonly int RequestToSetBackground = 101;
        MediaPlayer mediaPlayerCuddling, mediaPlayerButtonClick;



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);


            player = FindViewById<ImageView>(Resource.Id.imgPlayer);
            computer = FindViewById<ImageView>(Resource.Id.imgComp);
            paper = FindViewById<ImageView>(Resource.Id.imgPaper);
            stone = FindViewById<ImageView>(Resource.Id.imgStone);
            scissors = FindViewById<ImageView>(Resource.Id.imgScissors);
            play = FindViewById<Button>(Resource.Id.btnPlay);
            scoreBoard = FindViewById<ImageView>(Resource.Id.imgScoreBoard);
            edit = FindViewById<ImageView>(Resource.Id.imgEdit);
            root = FindViewById<RelativeLayout>(Resource.Id.llRoot);
            currentScore = FindViewById<TextView>(Resource.Id.lblcurrentScores);
            currentLives = FindViewById<TextView>(Resource.Id.lblcurrentLives);
            mediaPlayerCuddling = MediaPlayer.Create(this, Resource.Raw.cuddling);
            mediaPlayerButtonClick = MediaPlayer.Create(this, Resource.Raw.buttonClick);


            paper.Enabled = false;
            stone.Enabled = false;
            scissors.Enabled = false;

            play.Click += Play_Click;
            paper.Click += Paper_Click;
            stone.Click += Stone_Click;
            scissors.Click += Scissors_Click;
            scoreBoard.Click += ScoreBoard_Click;
            edit.Click += Edit_Click;


            FindViewById<ImageView>(Resource.Id.imgGooglePlay).Click += delegate
            {
                Intent browserIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("https://play.google.com/store/apps"));

                browserIntent.AddFlags(ActivityFlags.NewTask);
                StartActivity(browserIntent);
            };

            FindViewById<ImageView>(Resource.Id.imgExit).Click += (s, e) =>
            {
                FinishAndRemoveTask();
            };

        }


        private void ScoreBoard_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ScoreBoardActivity));
        }

        private void Scissors_Click(object sender, EventArgs e)
        {
            if (scissors.Enabled == true)
            {
                mediaPlayerButtonClick.Start();
            }
            userchoice = "scissors";
            computerchoice = ComputerChoice();
            Game(userchoice, computerchoice);
            player.SetImageResource(Resource.Drawable.scissors);
        }

        private void Stone_Click(object sender, EventArgs e)
        {
            if (stone.Enabled == true)
            {
                mediaPlayerButtonClick.Start();
            }
            userchoice = "stone";
            computerchoice = ComputerChoice();
            Game(userchoice, computerchoice);
            player.SetImageResource(Resource.Drawable.stone);
        }

        private void Paper_Click(object sender, EventArgs e)
        {
            if (paper.Enabled == true)
            {
                mediaPlayerButtonClick.Start();
            }
            
            userchoice = "paper";
            computerchoice = ComputerChoice();
            Game(userchoice, computerchoice);
            player.SetImageResource(Resource.Drawable.paper2);

        }

        private void Play_Click(object sender, EventArgs e)
        {
            
            play.Visibility = ViewStates.Invisible;
            paper.Enabled = true;
            stone.Enabled = true;
            scissors.Enabled = true;
            myLives = 3;
            currentLives.Text = myLives.ToString();
            playerScore = 0;
            currentScore.Text = playerScore.ToString();
        }

        public string ComputerChoice()
        {
            int number = random.Next(1, 4);

            if (number == 1)
            {
                computer.SetImageResource(Resource.Drawable.paper2);
                return "paper";                
            }
            else if (number == 2)
            {
                computer.SetImageResource(Resource.Drawable.stone);
                return "stone";
            }
            else
            {
                computer.SetImageResource(Resource.Drawable.scissors);
                return "scissors";

            }
        }

        public void Game(string playerchoice, string computerchoice)
        {
            Scores sc = new Scores();
            int id = sc.ScoreID;
           
            if ((playerchoice == "paper" && computerchoice == "stone") || (playerchoice == "stone" && computerchoice == "scissors") || (playerchoice == "scissors" && computerchoice == "paper"))
                {
                    playerScore += 10;
                    currentScore.Text = playerScore.ToString();

                    Toast message = Toast.MakeText(this, $"Yeah",
                                                        ToastLength.Long);
                    message.Show();
                }
             else if (playerchoice == computerchoice)
                {
                    playerScore += 5;
                    currentScore.Text = playerScore.ToString();

                    Toast message = Toast.MakeText(this, $"Not bad!",
                                                        ToastLength.Long);
                    message.Show();
                }
             else if(myLives>1)
                {
                myLives -= 1;
                currentLives.Text = myLives.ToString();
                Toast message = Toast.MakeText(this, $"Oops",
                                                ToastLength.Long);
                message.Show();
                }
            else
            {
                myLives -= 1;
                currentLives.Text = myLives.ToString();
                mediaPlayerCuddling.Start();
                Toast finalMessage = Toast.MakeText(this, $"Game over!",
                                               ToastLength.Long);

                
                finalMessage.Show();
                Insert(id,currentDate, playerScore);
                DeleteFromDB();
                play.Visibility = ViewStates.Visible;
                paper.Enabled = false;
                stone.Enabled = false;
                scissors.Enabled = false;
                
            }

        }

        public void Insert(int id, string date, int score)
        {            
            var connection = new SQLiteConnection(dbPath);
            //connection.DropTable<Scores>();
            connection.CreateTable<Scores>();
            Scores newScore = new Scores(id, date, score);
            connection.Insert(newScore);
        }

        public void DeleteFromDB()
        {
            var connection = new SQLiteConnection(dbPath);

            var record = (from records in connection.Table<Scores>()
                          orderby records.Score ascending
                          select records).First();

            var allRecord = connection.Table<Scores>().Count();

            if (allRecord > 5)
            {
                connection.Delete(record);

            }

        }


        private void Edit_Click(object sender, EventArgs e)
        {
            Intent editBackground = new Intent(this, typeof(EditBackgroundActivity));

            StartActivityForResult(editBackground, RequestToSetBackground);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {

            if (!isBackGroundSet(requestCode, resultCode))
            {
                throw new Exception();
            }
            string strNewBackground = data.GetStringExtra("newBackground");

            if (strNewBackground.Equals("Yellow"))
            {
                root.SetBackgroundResource(Resource.Drawable.yellowbackground);

            }
            else if (strNewBackground.Equals("Blue"))
            {
                root.SetBackgroundResource(Resource.Drawable.bluebackground);

            }
            else if (strNewBackground.Equals("Green"))
            {
                root.SetBackgroundResource(Resource.Drawable.greenbackground);
            }
            else if (strNewBackground.Equals("Red"))
            {
                root.SetBackgroundResource(Resource.Drawable.redbackground);
            }
            else
            {
                root.SetBackgroundResource(Resource.Drawable.brownbackground);
            }
        }

        private bool isBackGroundSet(int requestCode, Result resultCode)
        {
            return requestCode == 101 && resultCode == Result.Ok;
        }
    }
}

